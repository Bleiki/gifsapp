//
//  GifTableViewCell.swift
//  GifsApp
//
//  Created by Vista on 16.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class GifTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var gifImageView: UIImageView! //For GIF.
    @IBOutlet weak var titleLabel: UILabel! //For GIF title.
    
    //MARK: - Properties
    var activityIndicator: UIActivityIndicatorView? //For visualize the load.
    var request: String? //For check URL of GIF.
    
    //MARK: - SupportMethods
    func createAndRunActivityIndicator() {
        
        if self.activityIndicator == nil {
            
            let actIndView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            
            actIndView.center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
            actIndView.startAnimating()
            
            self.addSubview(actIndView)
            self.bringSubview(toFront: actIndView)
            
            self.activityIndicator = actIndView
            
        } else {
            
            self.activityIndicator?.startAnimating()
            self.bringSubview(toFront: self.activityIndicator!)
        }
    }
    
    func stopActivityIndicator() {
        
        if self.activityIndicator != nil {
            
            self.sendSubview(toBack: self.activityIndicator!)
            self.activityIndicator?.stopAnimating()
        }
    }
}
