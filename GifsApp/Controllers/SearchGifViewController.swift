//
//  SearchGifViewController.swift
//  GifsApp
//
//  Created by Vista on 16.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class SearchGifsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var activityIndicator: UIActivityIndicatorView?
    var messageLabel: UILabel? //Message for empty gifs.
    
    let gifHeight: CGFloat = 220
    var gifs = [Gif]()
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.settingSubviews()
    }
    
    //MARK: - UITableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gifs.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.gifHeight
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "GifCell"
        let gif        = self.gifs[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! GifTableViewCell
        
        cell.request            = gif.imageURL //For check GIF URL.
        cell.titleLabel.text    = gif.title
        cell.gifImageView.image = nil //For reset the image.
        
        self.getImageFor(gif: gif, in: cell)
        
        return cell
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.gifs.count - 1 {
            self.search()
        }
    }
    
    //MARK: - SupportMethods
    fileprivate func settingSubviews() {
        
        self.navigationItem.title  = "GIFs"
        self.searchBar.placeholder = "Search..."
        self.tableView.tableFooterView = UIView()
        
        //Set color for placeholder text.
        if let textField = self.searchBar.value(forKey: "searchField") as? UITextField {
            if let label = textField.value(forKey: "placeholderLabel") as? UILabel {
                
                label.textColor = .black
            }
            
            //Set color for search image.
            if let iconView = textField.leftView as? UIImageView {
                
                iconView.image     = iconView.image?.withRenderingMode(.alwaysTemplate)
                iconView.tintColor = .black
            }
        }
        
        //Set color for cancel btn.
        for view in self.searchBar.subviews[0].subviews {
            if let btn = view as? UIButton {
                
                btn.setTitleColor(.black, for: .normal)
            }
        }
    }
    
    func search() {
        
        if let text = self.searchBar.text, !text.characters.isEmpty {
            
            self.createAndRunActivityIndicator()
            
            GifDataManager.instance.searchGifsAt(text: text, offset: gifs.count, success: { (gifs) in
                
                self.gifs = gifs
                
                self.tableView.reloadData()
                
                self.checkGIFs()
                self.stopActivityIndicator()
                
            }, failure: { (error) in
                
                self.checkGIFs()
                self.stopActivityIndicator()
                
                print(error?.localizedDescription ?? "Can't get gifs!!!")
            })
        }
    }
    
    fileprivate func getImageFor(gif: Gif, in cell: GifTableViewCell) {
        
        if let url = gif.imageURL {
            
            cell.createAndRunActivityIndicator()
            
            GifDataManager.instance.getGifAt(url: url, success: { (image) in
                
                if cell.request == url {
                    cell.gifImageView.image = image
                    cell.stopActivityIndicator()
                }
            })
        }
    }
}
