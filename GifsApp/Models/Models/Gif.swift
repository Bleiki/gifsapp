//
//  Gif.swift
//  GifsApp
//
//  Created by Vista on 16.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import Foundation

class Gif {
    
    //MARK: - Properties
    var id: String?
    var title: String?
    var imageURL: String?

    //MARK: - LifeCycle
    init(dataResponse: [String : Any]) {
        
        self.id    = dataResponse["id"] as? String
        self.title = dataResponse["title"] as? String
        
        if let images = dataResponse["images"] as? [String : Any], let fixedHeight = images["fixed_height"] as? [String : Any] {
            self.imageURL = fixedHeight["url"] as? String
        }
    }
}
