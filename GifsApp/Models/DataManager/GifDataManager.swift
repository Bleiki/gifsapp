//
//  GifDataManager.swift
//  GifsApp
//
//  Created by Vista on 16.10.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import Foundation
import Alamofire
import ImageIO

class GifDataManager {
    
    //MARK: - Properties
    static let instance = GifDataManager()
    
    fileprivate let apiKey  = "ygaiNA2MkSSatc5yHxrsuLqHlTqnXXHs" //Unique identifier for app.
    fileprivate let baseURL = "https://api.giphy.com"
    fileprivate let successSttusCode = 200
    
    //MARK: - ServerMethods
    func searchGifsAt(text: String, offset: Int, success: @escaping (_ albums: [Gif]) -> (), failure: @escaping (_ error: Error?) -> ()) {
        
        let url        = self.baseURL + "/v1/gifs/search"
        var parameters = [String : Any]()
        
        parameters["q"]       = text
        parameters["offset"]  = offset
        parameters["api_key"] = self.apiKey
        
        Alamofire.request(
            url,
            method: .get,
            parameters: parameters,
            encoding: URLEncoding.default,
            headers: nil).responseJSON { (dataResponse) in
                
                if dataResponse.response?.statusCode == self.successSttusCode {
                    if let result = dataResponse.result.value as? [String : Any], let arrayOfGifs = result["data"] as? [[String : Any]] {
                        
                        var gifs = [Gif]()
                        
                        for gifParams in arrayOfGifs {
                            
                            let gif = Gif(dataResponse: gifParams)
                            gifs.append(gif)
                        }

                        success(gifs)

                    } else {
                        failure(dataResponse.error)
                    }
                } else {
                    failure(dataResponse.error)
                }
        }
    }
    
    func getGifAt(url: String, success: @escaping (_ image: UIImage?) -> ()) {
        
        Alamofire.request(url).responseData { (dataResponse) in
            
            if let data = dataResponse.data as NSData? {
                
                let image = UIImage.gifImageWithData(data: data)
                success(image)
                
            } else {
                print("Can't get image at URL: \(url)")
            }
        }
    }
}
